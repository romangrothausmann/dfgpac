################################################################################
# base system
################################################################################
FROM ubuntu:16.04 as system


################################################################################
# builder
################################################################################
FROM system as builder

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    ca-certificates `# essential for git over https` \
    build-essential pkg-config g++ git scons cmake yasm

RUN apt-get update && apt-get install -y --no-install-recommends \
    zlib1g-dev libfreetype6-dev libjpeg62-dev libpng-dev libmad0-dev libfaad-dev libogg-dev libvorbis-dev libtheora-dev liba52-0.7.4-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev libavdevice-dev libxv-dev x11proto-video-dev libgl1-mesa-dev x11proto-gl-dev libxvidcore-dev libssl-dev libjack-dev libasound2-dev libpulse-dev libsdl2-dev dvb-apps mesa-utils


RUN git clone --depth 1             https://github.com/gpac/gpac gpac_public
RUN git clone --depth 1             https://github.com/gpac/deps_unix

RUN cd deps_unix && \
    git submodule update --init --recursive --force --checkout && \
    ./build_all.sh linux

RUN cd gpac_public && \
    ./configure && \
    make -j"$(nproc)" && \
    make -j"$(nproc)" install

RUN mkdir /work

WORKDIR /work
